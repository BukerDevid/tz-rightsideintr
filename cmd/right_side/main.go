package main

import (
	"os"
	"os/signal"
	"right_side/internal/service"
	"syscall"

	"github.com/sirupsen/logrus"
)

func main() {
	var quit = make(chan os.Signal, 1)
	osKillSignals := []os.Signal{
		syscall.SIGKILL,
		syscall.SIGTERM,
		syscall.SIGHUP,
		syscall.SIGINT,
	}
	signal.Notify(quit, osKillSignals...)

	if err := service.Service(quit); err != nil {
		logrus.Error(err.Error())
	}
}
