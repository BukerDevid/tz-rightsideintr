# Техническое задание

## Разработать API в котором 3 эндпоинта

* Есть одна сущность
* При старте сервера все записи из базы подгружаются в оперативку,

## Реализовать:

* Эндпоинт удаления - удаляет в базе запись
* Эндпоинт изменения - изменяет в базе запись
* Эндпоинт показать - показывает текущий актуальный список
* Процесс, который подгружает записи с базы в оперативку, с определенным интервалом


# `Реализация`

## Запуск
Для запуска сервиса:

```
    docker-compose up -d
```

## Инструментарий

Инструментарии разработки:
* Docker
* Docker-compose
* Golang
* MongoDB

Драйверы:
* Route http -> `gorilla/mux`
* Route handler -> `gorilla/handler` (for CORS)
* MongoDB -> `mongo`
* TimeScheduler -> `cron`

## API

### `GET /units` - Для получения всех элементов из памяти сервиса

### `POST /units` - Запись элементов в БД
Возможна запись любой структуры, как пример `[{"person":"пользователь"}]` 

```
    curl --header "Content-Type: application/json" \
        --request POST \
        --data '[{"person":"admin"},{"person":"test"}]' \
        http://localhost:4080/units
```

### `UPDATE /units` - Обновление элементов в БД

```
    curl --header "Content-Type: application/json" \
        --request UPDATE \
        --data '[{"id":"идентификатор записи","value":{"person":"New admin1"}},{"id":"идентификатор записи","value":{"person":"check update test"}}]' \
        http://localhost:4080/units
```

### `DELETE /units` - Удаление элементов в БД
```
    curl --header "Content-Type: application/json" \
        --request DELETE \
        --data '[{"id":"идентификатор записи"},{"id":"идентификатор записи"}]' \
        http://localhost:4080/units
```