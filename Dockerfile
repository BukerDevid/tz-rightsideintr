# Builer образ для сборки.
FROM golang:latest AS build
WORKDIR /go/src/service
COPY . .
RUN go mod tidy && \
    go build -o /right_side ./cmd/right_side/main.go

# Service образ без артифактов и мусора.
FROM golang:latest
LABEL maintainer="mr.wgw@yandex.ru"
COPY --from=build /right_side /bin
CMD ["right_side"]