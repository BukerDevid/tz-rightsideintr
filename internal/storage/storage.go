package storage

import (
	"fmt"
	"right_side/pkg/database"
)

var db *database.Database
var nameDatabase string
var nameCollection string

func SetProvider(dbConn *database.Database, dbName, dbCollection string) error {
	if dbConn != nil {
		db = dbConn
		nameDatabase = dbName
		nameCollection = dbCollection

		return nil
	}

	return fmt.Errorf("db connect is nil")
}
