package storage

import (
	"right_side/internal/models"

	"github.com/sirupsen/logrus"
)

var memStorage = []models.Unit{}

func GetUnits() []models.Unit {
	return memStorage
}

func SyncUnits() {
	val, err := ReadAllUnits()
	if err != nil {
		logrus.Errorf("sync failed.\n%v", err)
		return
	}
	memStorage = val
}
