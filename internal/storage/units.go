package storage

import (
	"context"
	"right_side/internal/models"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

// Используешь database.Connect()?
// Используй и defer database.Relise()!

func WriteUnits(units []interface{}) error {
	connect := db.Connect()
	defer db.Relise()

	_, err := connect.Database(nameDatabase).Collection(nameCollection).
		InsertMany(context.Background(), units)

	return err
}

func UpdateUnit(units []models.Unit) error {
	connect := db.Connect()
	defer db.Relise()

	for _, unit := range units {
		_, err := connect.Database(nameDatabase).Collection(nameCollection).
			UpdateOne(
				context.Background(),
				bson.D{{"id", unit.ID}},
				bson.D{{"$set", unit}},
			)
		if err == mongo.ErrNoDocuments {
			continue
		} else if err != nil {
			logrus.Error(err)
			return err
		}
	}

	return nil
}

func DeleteUnit(units []models.Unit) error {
	connect := db.Connect()
	defer db.Relise()

	for _, unit := range units {
		_, err := connect.Database(nameDatabase).Collection(nameCollection).
			DeleteOne(
				context.Background(),
				bson.D{{"id", unit.ID}},
			)
		if err == mongo.ErrNoDocuments {
			continue
		} else if err != nil {
			logrus.Error(err)
			return err
		}
	}

	return nil
}

func ReadAllUnits() ([]models.Unit, error) {
	connect := db.Connect()
	defer db.Relise()

	cursor, err := connect.Database(nameDatabase).Collection(nameCollection).Find(context.Background(), bson.D{{}})
	if err == mongo.ErrNoDocuments {
		return make([]models.Unit, 0), nil

	} else if err != nil {
		return nil, err
	}

	var units = make([]models.Unit, cursor.RemainingBatchLength())

	if cursor.RemainingBatchLength() == 0 {
		return units, nil
	}

	counter := 0
	for cursor.Next(context.Background()) {
		unit := &models.Unit{}

		err := cursor.Decode(unit)
		if err != nil {
			continue
		}

		units[counter] = *unit
		counter++
	}

	return units, nil
}
