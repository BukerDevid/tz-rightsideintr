package service

import (
	"os"
	"right_side/internal/rest"
	"right_side/internal/storage"
	"right_side/pkg/database"
	"right_side/pkg/loggo"
	"right_side/pkg/server"
	"right_side/pkg/utility"
	"time"

	"github.com/robfig/cron/v3"
	"github.com/sirupsen/logrus"
)

// Пример функции в качестве callback
func ServerHttpCallback() {
	logrus.Info("Server state is down")
}

func Service(quit chan os.Signal) error {
	logrus.Info("Service")

	// Use export DRIVER_CONFIG = {json,*yaml,toml}
	// * - default
	// set path in env PATH_CONFIG=/configs/config.json for docker-compose
	config, err := utility.ReadConfig(os.Getenv("PATH_CONFIG"))
	if err != nil {
		return err
	}

	//
	// Logger
	//
	var level logrus.Level
	switch config.Service.Debug {
	case "InfoLevel":
		{
			level = logrus.InfoLevel
		}
	case "TraceLevel":
		{
			level = logrus.TraceLevel
		}
	default:
		{
			logrus.Warn("Debug level invalid. Set default TraceLevel")
			level = logrus.TraceLevel
		}
	}
	loggo.InitCastomLogger(&logrus.JSONFormatter{TimestampFormat: "15:04:05 02/01/2006"}, level, false, true)

	//
	// HTTP Server
	//
	logrus.Infof("Base service version: %s", config.Service.Version)
	if _, err = server.NewServer(
		nil,
		config.Service.Address,
		config.Service.Port,
		ServerHttpCallback,
		rest.InitHandlers(),
	); err != nil {
		return err
	}

	//
	// Database provider
	//
	logrus.Info("Connecting to database")
	var db *database.Database
	for i := 0; true; i++ {
		db, err = database.NewProvider(config.Database.Host, config.Database.Port)
		if err == nil {
			break
		} else if i == 10 && err != nil {
			logrus.Fatal(err)
		}

		logrus.Info("...")
		time.Sleep(time.Second * 2)
	}
	logrus.Info("Database connect success")

	storage.SetProvider(
		db,
		config.Database.Database,
		config.Database.Collection,
	)
	defer db.ReliseProvider()

	//
	// Scheduler manager
	//
	logrus.Info("Starting scheduler")
	scheduler := cron.New(cron.WithSeconds())

	if _, err = scheduler.AddFunc(config.Cron.Times, func() {
		logrus.Debug("[EVENT] sync state")
		storage.SyncUnits()

	}); err != nil {
		return err
	}

	scheduler.Start()
	defer scheduler.Stop()

	<-quit

	return nil
}
