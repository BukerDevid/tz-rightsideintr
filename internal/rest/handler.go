package rest

import (
	"encoding/json"
	"fmt"
	"net/http"
	"right_side/internal/models"
	"right_side/internal/storage"

	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

type MResponse struct {
	OK     bool        `json:"ok"`
	Result interface{} `json:"result,omitempty"`
	Error  interface{} `json:"error,omitempty"`
}

func writeRequest(w http.ResponseWriter, data interface{}) {
	var body []byte

	if val, ok := data.(error); ok {
		body, _ = json.Marshal(&MResponse{
			OK:    false,
			Error: val.Error(),
		})

	} else {
		body, _ = json.Marshal(&MResponse{
			OK:     true,
			Result: data,
		})

	}

	w.Write(body)
}

func infoHandler(w http.ResponseWriter, r *http.Request) {
	writeRequest(w, "Server RightSide")
}

func getAllUnits(w http.ResponseWriter, r *http.Request) {
	writeRequest(w, storage.GetUnits())
}

func setUnits(w http.ResponseWriter, r *http.Request) {
	read := []interface{}{}

	if err := json.NewDecoder(r.Body).Decode(&read); err != nil {
		writeRequest(w, fmt.Errorf("failed read body"))
		logrus.Debug(err)
		return
	}

	units := make([]interface{}, len(read))

	logrus.Debug("Units read: ", read)
	for idx, val := range read {
		units[idx] = models.Unit{
			ID:    uuid.New().String(),
			Value: val,
		}
	}

	if err := storage.WriteUnits(units); err != nil {
		writeRequest(w, fmt.Errorf("failed write units"))
		return
	}

	writeRequest(w, "Success")
}

func updateUnits(w http.ResponseWriter, r *http.Request) {
	unit := []models.Unit{}
	if err := json.NewDecoder(r.Body).Decode(&unit); err != nil {
		writeRequest(w, fmt.Errorf("failed read body"))
		logrus.Debug(err)
		return
	}

	if err := storage.UpdateUnit(unit); err != nil {
		writeRequest(w, fmt.Errorf("failed update units"))
		return
	}
	writeRequest(w, "success")
}

func deleteUnits(w http.ResponseWriter, r *http.Request) {
	unit := []models.Unit{}
	if err := json.NewDecoder(r.Body).Decode(&unit); err != nil {
		writeRequest(w, fmt.Errorf("failed read body"))
		logrus.Debug(err)
		return
	}

	if err := storage.DeleteUnit(unit); err != nil {
		writeRequest(w, fmt.Errorf("failed update units"))
		return
	}
	writeRequest(w, "success")
}
