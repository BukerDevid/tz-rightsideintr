package rest

import (
	"github.com/gorilla/mux"
)

func InitHandlers() *mux.Router {
	route := mux.NewRouter()

	route.HandleFunc("/", infoHandler).Methods("GET")

	route.HandleFunc("/units", getAllUnits).Methods("GET")
	route.HandleFunc("/units", setUnits).Methods("POST")
	route.HandleFunc("/units", updateUnits).Methods("UPDATE")
	route.HandleFunc("/units", deleteUnits).Methods("DELETE")

	return route
}
