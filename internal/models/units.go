package models

type Unit struct {
	ID    string      `json:"id"`
	Value interface{} `json:"value,omitempty"`
}
