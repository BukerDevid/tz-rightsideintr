package database

import (
	"testing"

	"go.mongodb.org/mongo-driver/mongo"
)

var Connect_M *Database

func TestDatabase(t *testing.T) {
	Connect_M := &Database{
		client: &mongo.Client{},
	}

	t.Run("Connect test", func(t *testing.T) {
		Connect_M.Connect()
		if !Connect_M.CheckLock() {
			t.Fatal("Failed lock!")
		}
	})

	t.Run("Relise test", func(t *testing.T) {
		Connect_M.Relise()
		if Connect_M.CheckLock() {
			t.Fatal("Failed unlock!")
		}
	})
}
