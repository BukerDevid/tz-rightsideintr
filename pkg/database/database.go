package database

import (
	"context"
	"fmt"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Database struct {
	mx     sync.RWMutex
	client *mongo.Client
}

func NewProvider(host string, port int) (*Database, error) {
	logrus.Debug("connecting by url: ", fmt.Sprintf("mongodb://%s:%d", host, port))
	client, err := mongo.Connect(
		context.Background(),
		options.Client().ApplyURI(
			fmt.Sprintf("mongodb://%s:%d", host, port),
		))
	if err != nil {
		return nil, fmt.Errorf("failed connect to database:\n %v", err)
	}

	ctx, cancelPing := context.WithTimeout(context.Background(), time.Second*3)
	defer cancelPing()

	if err := client.Ping(ctx, nil); err != nil {
		return nil, fmt.Errorf("failed connect to database:\n %v", err)
	}

	return &Database{client: client}, nil
}

func (db *Database) ReliseProvider() error {
	if db.client != nil {
		return db.client.Disconnect(context.Background())
	}

	return nil
}

func (db *Database) CheckLock() bool {
	return !db.mx.TryLock()
}

func (db *Database) Connect() *mongo.Client {
	if db != nil {
		db.mx.Lock()
		return db.client
	}
	return nil
}

func (db *Database) Relise() {
	if db != nil {
		db.mx.Unlock()
	}
}
