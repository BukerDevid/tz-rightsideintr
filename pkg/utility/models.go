package utility

// ConfigModel конфигурации сервиса
type ConfigModel struct {
	Service struct {
		Version string `json:"version" yaml:"version"`
		Address string `json:"address" yaml:"address"`
		Port    int    `json:"port" yaml:"port"`
		Debug   string `json:"debug" yaml:"debug"`
	} `json:"service" yaml:"service" toml:"service"`
	Database struct {
		Host       string `json:"host" yaml:"host"`
		Port       int    `json:"port" yaml:"port"`
		Database   string `json:"database" yaml:"database"`
		Collection string `json:"collection" yaml:"collection"`
	} `json:"database" yaml:"database" toml:"database"`
	Cron struct {
		Times string `json:"times" yaml:"times"`
	} `json:"cron" yaml:"cron" toml:"cron"`
}
